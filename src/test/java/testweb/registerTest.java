package testweb;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import org.example.registerPage;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import reportes.extentFactory;

import java.time.Duration;

public class registerTest {
    public WebDriver driver;
    public WebDriverWait wait;
    static ExtentSparkReporter info = new ExtentSparkReporter("target/reporteRegistro.html");
    static ExtentReports extent;

    @BeforeAll
    public static void createReport(){
        extent = extentFactory.getInstance();
        extent.attachReporter(info);
    }
    @BeforeEach
    public void precondiciones(){
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, Duration.ofMillis(2000));
        registerPage registerPage = new registerPage(driver, wait);
        registerPage.setUp();
        registerPage.url("https://parabank.parasoft.com/parabank/register.htm;jsessionid=A1DADAFA906A637A761CF63B6FE5BBF1");
    }

    @Test
    public void RegistrarseTest() throws InterruptedException{
        registerPage registerPage = new registerPage(driver, wait);
ExtentTest test = extent.createTest("Registrandome como Luciana");
test.log(Status.INFO, "Iniciando el test de registro como Luciana");
registerPage.escribirNombre("Luciana");
Assertions.assertEquals("Luciana", "Luciana");
test.log(Status.PASS, "Escribiendo el nombre");
registerPage.escribirApellido("Costilla");
test.log(Status.PASS, "Escribiendo el apellido");
registerPage.escribirDireccion("Sherman Wallaby");
test.log(Status.PASS, "Escribiendo dirección.");
registerPage.escribirCiudad("Sidney");
test.log(Status.PASS, "Escribiendo ciudad.");
registerPage.escribirEstado("Buenos Aires.");
test.log(Status.PASS, "Escribiendo estado.");
registerPage.escribirCodigoPostal("S1185");
test.log(Status.PASS, "Escribiendo código postal.");
registerPage.escribirTelefono("536295648");
test.log(Status.PASS, "Escribiendo teléfono.");
registerPage.escribirSSN("betchabet@cha");
test.log(Status.PASS, "Escribiendo ssn.");
registerPage.escribirUsuario("lucianacostilla");
test.log(Status.PASS, "Escribiendo usuario.");
registerPage.escribirPassword("luciana12345");
test.log(Status.PASS, "Escribiendo contraseña.");
registerPage.escribirRepassword("luciana12345");
test.log(Status.PASS, "Confirmando contraseña.");
registerPage.clickearBotonRegistro();
test.log(Status.PASS, "Registro exitoso");
System.out.println(registerPage.visualizacion());
    }

    @AfterEach
    public void close(){
        registerPage registerPage = new registerPage(driver, wait);
       registerPage.close();
    }
    @AfterAll
    public static void guardarReporte(){
        extent.flush();
    }
}
