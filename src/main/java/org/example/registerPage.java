package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
/* Registro
Haga clic en <Registro>
Rellene el formulario de registro con los datos requeridos
Pulse de nuevo en <Registro>.
Compruebe que el texto "Su cuenta se ha creado correctamente. En la pantalla se puede ver "You are now logged in".
*/

public class registerPage extends basePage{
    public registerPage(WebDriver driver, WebDriverWait wait) {
            super(driver, null);
    }
    private By firstName = By.id("customer.firstName");
    private By lastName = By.id("customer.lastName");
    private By address = By.id("customer.address.street");
    private By city = By.id("customer.address.city");
    private By state = By.id("customer.address.city");
    private By zipcode = By.id("customer.address.zipCode");
    private By phone = By.id("customer.phoneNumber");
    private By SSN = By.id("customer.ssn");
    private By username = By.id("customer.username");
    private By password = By.id("customer.password");
    private By passwordConfirm = By.id("repeatedPassword");
    private By button = By.className("button");
    private By mensajeBienvenida = By.cssSelector("#rightPanel .title");
    private By mensajeInicioSesion = By.cssSelector("#rightPanel p");

public void escribirNombre(String name) throws InterruptedException{
    this.sendText(name, firstName);
}
public void escribirApellido(String surname) throws InterruptedException{
    this.sendText(surname, lastName);
}
public void escribirDireccion(String direccion) throws InterruptedException{
    this.sendText(direccion, address);
}
public void escribirCiudad(String ciudad) throws InterruptedException{
    this.sendText(ciudad, city);
}
public void escribirEstado(String estado) throws InterruptedException{
    this.sendText(estado, state);
}
public void escribirCodigoPostal(String codigoPostal) throws InterruptedException{
    this.sendText(codigoPostal, zipcode);
}
public void escribirTelefono(String telefono) throws InterruptedException{
    this.sendText(telefono, phone);
}
public void escribirSSN(String mail) throws InterruptedException{
    this.sendText(mail, SSN);
}
public void escribirUsuario(String usuario) throws InterruptedException{
    this.sendText(usuario, username);
}
public void escribirPassword(String contrasenia) throws InterruptedException{
    this.sendText(contrasenia, password);
    }
public void escribirRepassword(String recontrasenia)throws InterruptedException{
    this.sendText(recontrasenia, passwordConfirm);
}
public void clickearBotonRegistro() throws InterruptedException{
    this.click(button);
    }
    public String visualizacion() throws InterruptedException {
    return "El mensaje de bienvenida es: " + this.getText(mensajeBienvenida) + " " + mensajeInicioSesion;}
}
