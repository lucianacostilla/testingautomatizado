package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
/*
Pruebas Front-end:
Iniciar sesión.
 */

public class loginPage extends basePage{
    private By username = By.name("username");
    private By password = By.name("password");
    private By loginbutton = By.className("button");
   /* private By emailRequerido = By.xpath("//*[@id=\"root\"]/main/div/form/div[1]/small");
    private By passwordRequerida = By.xpath("//*[@id=\"root\"]/main/div/form/div[2]/small");
    private By credencialesIncorrectas = By.className("form-feedback");
    private By saludo = By.className("txt-hola");
    private By nombreSaludo = By.name("txt-nombre");*/
    public loginPage(WebDriver driver, WebDriverWait wait) {
        super(driver, null);
    }

    public void escribirUsuario(String usuario) throws InterruptedException{
        this.sendText(usuario, username);
    }
    public void escribirContrasenia(String contrasenia) throws InterruptedException{
        this.sendText(contrasenia, password);
    }
    public void iniciarSesion() throws InterruptedException{
        this.click(loginbutton);
    }
   /* public String correoObligatorio() throws InterruptedException{
        return "Error: " + this.getText(emailRequerido);
    }
    public String contraseñaObligatoria() throws InterruptedException{
        return "Error: " + this.getText(passwordRequerida);
    }
    public String saludo() throws InterruptedException{
        return "Mensaje de saludo: " + this.getText(saludo);
    }
    public String nombreSaludo() throws InterruptedException{
        return "Nombre en el saludo: " + this.getText(nombreSaludo);
    }
    public String credencialesInv() throws InterruptedException{
        return "Error: " + this.getText(credencialesIncorrectas);
    }*/

}
