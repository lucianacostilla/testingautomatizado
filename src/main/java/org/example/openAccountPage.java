package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

/*
Abrir una nueva cuenta
Haga clic en <Abrir nueva cuenta>.
En el apartado "¿Qué tipo de cuenta desea abrir?" seleccione la opción <SAVINGS>.
Haga clic en <Abrir nueva cuenta>
Compruebe si el texto "Congratulations, your account is now open." está visible en la pantalla
*/
public class openAccountPage extends basePage{
    public openAccountPage(
    WebDriver driver, WebDriverWait wait) {
        super(driver, null);
    }
    private By abrirCuenta = By.xpath("//*[@id=\"leftPanel\"]/ul/li[1]/a");
    private By opcionCuenta = By.xpath("//*[@id=\"type\"]");
    private By botonCrearCuenta = By.xpath("//*[@id=\"type\"]");

}
